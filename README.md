# Data driven chemistry

This repository contains the course materials for the Year 2 chemistry course in "Data-driven chemistry" (DDC).

## Session topics

1. Introduction to Jupyter and Problem Solving
2. Variables
3. Loops
4. Functions
5. Plotting and Statistics
6. Statistics 2
7. Curve fitting
8. Applications in chemistry I
9. Applications in chemistry II
10. Analysing data from start to finish

## Learning Outcomes

On successful completion of this course, you should be able to:
- Perform numerical operations such as vector algebra and calculate simple statistics on data sets.
- Write readable, well-documented and modular code.
- Break a problem into logical steps, and use loops and decision operations to solve tasks.
- Import and clean experimental data, and choose the appropriate variable types to hold information.
- Fit models to numerical data, and plot the results in a number of different formats.