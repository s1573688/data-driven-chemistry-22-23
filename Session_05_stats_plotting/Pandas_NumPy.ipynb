{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Loading Structured Data and Accessing it in the Array\n",
    "\n",
    "### extra material for the Session 5\n",
    "\n",
    "by Valentina Erastova <valentina.erastova@ed.ac.uk> University of Edinburgh\n",
    "\n",
    "Thanks to Hannah Pollak (our demonstrator) for help developing this\n",
    "\n",
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### *When shall I use `numpy` and when `pandas` is better?...*\n",
    "<a class=\"anchor\" id=\"numpy_pandas\"></a>\n",
    "    \n",
    "In python, there are multiple ways to load and work with datafiles.\n",
    "    \n",
    "One possible option is to use [**`numpy.loadtxt`**](https://numpy.org/doc/stable/reference/generated/numpy.loadtxt.html) function to read textfile data into a numpy array. Following the link we can check how `loadtxt` expects a file to be formatted. The comment lines should starts  with `#` and the values within one row are expected to be separated by a \"whitespace\" (=one or multiple space or tab characters). For example a file like this:\n",
    " \n",
    "```\n",
    "# AUTHORS: Schmidt J, Institute of Plant Biochemistry, Halle, Germany\n",
    "# m/z int. rel.int.\n",
    "  27.000 64.627 5\n",
    "  30.000 121.469 11\n",
    "  32.000 45.386 3\n",
    "  41.000 16.304 0\n",
    "  42.000 3731.328 372\n",
    "  44.000 29.229 1\n",
    "```\n",
    "will be read in correct. BUT, if the formatting of the file differs from the expected structure,ni.e. if in the `filename.txt` values are separated by `,` instead of a whitespace, or if lines were commented with `;` instead of `#`, the file would only be read in correctly if the delimeters and comments are stated: \n",
    "\n",
    "```python\n",
    "np.loadtxt('filename.txt', delimiter=',', comments=';')\n",
    "```\n",
    "    \n",
    "Another way is to store a textfile data in a **pandas DataFrame**. In this case, we have to use [`pandas.load_csv`](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.read_csv.html). Following the link to their documentation we can see that the function expects the row values to be separated by `,` and by default it will not expect any commented lines. \n",
    "\n",
    "\n",
    "### Viewing the datafile in the Jupyter Notebook \n",
    "<a class=\"anchor\" id=\"head\"></a>\n",
    "\n",
    "Before loading the data into the notebook it is good to take a closer look at its structure. \n",
    "\n",
    "To do this we can use `head`. Since this command is not a python command we have to add a `!` before it. To only see first 24 lines, add `-24` before filename `filename.txt`.\n",
    "\n",
    "```python\n",
    "!head -24 filename.txt\n",
    "```\n",
    "Another command you may come across is `cat` - use it in the similar way.\n",
    "\n",
    "```python\n",
    "!cat -24 filename.txt\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a class=\"anchor\" id=\"Practice2\"></a>\n",
    "<div class=\"alert alert-info\">\n",
    "<b>PRACTICE - Loading Data from a File </b>    \n",
    "    \n",
    "Let's try to load an example file ```array_test.txt``` with pandas and assign it into a dataframe. \n",
    "    \n",
    "First, check how the file looks like with ```!head``` then load the file (do you need to import pandas?)\n",
    "    \n",
    "```python\n",
    "filename = 'DATA/array_test.txt'\n",
    "example_df = pd.read_csv(filename)\n",
    "example_df.head(5)\n",
    "```    \n",
    "</div>\n",
    "     "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "#!head 'array_test.txt'\n",
    "!cat 'DATA/array_test.txt'"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# import pandas as pd\n",
    "# import numpy as np\n",
    "\n",
    "# load the file \n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-info\">\n",
    "    \n",
    "As we can see, ```read_csv``` was not able to correctly read the file. Therefore, we have to provide ```read_csv``` with the correct ```delimiter``` and ```comment``` information. If comments are only at the beginning of the file, alternatively, they could also be skipped with ```skiprows```. Compared to ```loadtxt```, ```read_csv``` is much more flexible when it comes to reading files which are formatted in a non-standard way. By default, ```read_csv``` uses the first row for column lables. If this is not desired you have to specify ```header=None```.\n",
    "\n",
    "```python\n",
    "example_df = pd.read_csv(filename, comment='#', delimiter='\\s+', header=None)\n",
    "```\n",
    "    \n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "# change the way you read the header\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-info\">\n",
    "    \n",
    "Compared to `loadtxt`, `read_csv` is much more flexible when it comes to reading files which are formatted in a non-standard way. Additionally, DataFrames give the possibility to assign column labels and an index which can make data accession more intuitive. This makes them especially useful for large datasets. If no information is given, the first row will be used as column names and the index will be just the row numbers.\n",
    "\n",
    "Let's assign column names and an index:\n",
    "\n",
    "```python\n",
    "example_df.columns=['c0', 'c1', 'c2', 'c3', 'c4', 'c5', 'c6', 'c7']\n",
    "example_df.index=['a', 'b', 'c', 'd', 'e']\n",
    "```\n",
    "    \n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# assign columns and index\n",
    "\n",
    "filename = 'DATA/array_test.txt'\n",
    "example_df = pd.read_csv(filename)\n",
    "example_df.head(5)\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-info\">\n",
    "    \n",
    "Can you read the other file ```array_test2.txt```in the same way as```array_test.txt```? Or do you need to provide the correct ```delimiter``` and ```comment``` information? What about the header?\n",
    "    \n",
    "Check the file and decide.\n",
    "\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# read in another file\n",
    "example2_df.index=['a', 'b', 'c', 'd', 'e']\n",
    "example2_df"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<details>\n",
    "<summary> <mark> EXAMPLE SOLUTION:</mark> </summary>\n",
    "\n",
    "\n",
    "Read in 'array_test.txt' file: \n",
    "\n",
    "```python\n",
    "import pandas as pd\n",
    "\n",
    "filename = 'DATA/array_test.txt'\n",
    "example_df = pd.read_csv(filename)\n",
    "example_df.head(5)\n",
    "    \n",
    "```\n",
    "\n",
    "Change the way you read the header:\n",
    "  \n",
    "```python    \n",
    "example_df = pd.read_csv(filename, comment='#', delimiter='\\s+', header=None)\n",
    "example_df\n",
    "    \n",
    "```\n",
    "   \n",
    "Assign columns and index:\n",
    "    \n",
    "```python\n",
    "example_df.columns=['c0', 'c1', 'c2', 'c3', 'c4', 'c5', 'c6', 'c7']\n",
    "example_df.index=['a', 'b', 'c', 'd', 'e']\n",
    "example_df\n",
    "\n",
    "    \n",
    "```\n",
    "    \n",
    "\n",
    "Read in 'array_test2.txt' file: \n",
    "    \n",
    "```python\n",
    "filename2 = 'DATA/array_test2.txt'\n",
    "example2_df=pd.read_csv(filename2, comment=';',delimiter=',')\n",
    "example2_df\n",
    "    \n",
    "```\n",
    "\n",
    "    \n",
    "    \n",
    "Note that this file has a header, so you do not need to assign the columns. It is still usefule to assign the index:\n",
    "    \n",
    "```python\n",
    "example2_df.index=['a', 'b', 'c', 'd', 'e']\n",
    "example2_df\n",
    "    \n",
    "```\n",
    "\n",
    "</details>\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Accessing the Data in an Array  \n",
    "\n",
    "In ```numpy```, different entries of an array can be accessed by specifying their row and column numbers. \n",
    "\n",
    "In ```pandas```, with `loc` and `iloc`, entries can be selected either by index and column label, or by the column and index numbers. \n",
    "\n",
    "**Remember:** When selecting data using row and column numbers, counting always starts from 0!\n",
    "\n",
    "\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "\n",
    "### Numpy VS Pandas CheatSheet <a class=\"anchor\" id=\"numpy_pandas_cheatsheet\"></a>\n",
    "\n",
    "\n",
    "<img src=\"FIGS/numpy_pandas_1.png\" width=\"700\">\n",
    "\n",
    "<br>\n",
    "\n",
    "\n",
    "\n",
    "\n",
    "\n",
    "\n",
    "</br>\n",
    " \n",
    "<img src=\"FIGS/numpy_pandas_2.png\" width=\"700\">\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
