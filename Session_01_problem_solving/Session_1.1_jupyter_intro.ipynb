{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Session 1: Problem Solving\n",
    "\n",
    "<a rel=\"license\" href=\"http://creativecommons.org/licenses/by/4.0/\"><img alt=\"Creative Commons Licence\" style=\"border-width:0\" src=\"https://i.creativecommons.org/l/by/4.0/88x31.png\" title='This work is licensed under a Creative Commons Attribution 4.0 International License.' align=\"right\"/></a>\n",
    "\n",
    "Author: Dr James Cumby   \n",
    "Email: james.cumby@ed.ac.uk"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Learning outcomes:\n",
    "\n",
    "By the end of this session, you should be able to:\n",
    "- Interact with a Jupyter notebook\n",
    "- break a complex problem into smaller steps;\n",
    "- consider how those steps might be implemented as code (developed more later in the course);\n",
    "- use pseudocode to develop simple algorithms\n",
    "\n",
    "We will be working through two notebooks:\n",
    "- [Session 1.1](Session_1.1_jupyter_intro.ipynb) -- Introduction to Jupyter notebooks (This notebook)\n",
    "- [Session 1.2](Session_1.2_problem_solving.ipynb) -- Problem solving and algorithms\n",
    "\n",
    "Some of the content is adapted from [Software carpentry lessons](http://swcarpentry.github.io/python-novice-gapminder/index.html).\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# 1.1 Getting Started with Jupyter notebooks "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "skip"
    }
   },
   "source": [
    "**Useful Jupyter Commands:**\n",
    "- to run the currently highlighted cell, hold <kbd>&#x21E7; Shift</kbd> and press <kbd>&#x23ce; Enter</kbd>;\n",
    "- to get help for a specific function, place the cursor within the function's brackets, hold <kbd>&#x21E7; Shift</kbd>, and press <kbd>&#x21E5; Tab</kbd>;"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "skip"
    }
   },
   "source": [
    "Over the next ten weeks you will learn how to use computer programming to help with chemistry. We will focus on Python: an easy-to-learn, but very powerful, programming language. As well as powering many of the apps on your phone, Python is widely used in many different disciplines. \n",
    "\n",
    "Python code can be run in many ways, but we will use Jupyter Notebooks like the one you're reading currently. Jupyter Notebooks allow you to combine live code and explanatory text in the same document, and can work like a digital lab book for computer code. \n",
    "\n",
    "Jupyter (and Python) is free and can be installed on your own computer, but for this course we will use the online [Noteable Service](https://www.ed.ac.uk/information-services/learning-technology/noteable) run by the University.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Notebook Layout"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "skip"
    }
   },
   "source": [
    "A Jupyter notebook consists of a series of **cells** which can contain either formatted text (a **Markdown cell**) or executable code (a **code cell**). The type of a cell can be changed using the toolbar at the top of the page.\n",
    "For example, if you click on this cell it will be highlighted by a box, and the toolbar will display 'Markdown':\n",
    "![Image showing 'Markdown' in drop-down box](images/markdown_dropdown.png)\n",
    "\n",
    "The cell below is a **code** cell. In addition to showing **Code** in the drop-down box (try clicking on it!), code cells also display the text `In []:` to the left-hand side. If the code cell has been executed, a number will appear between the square brackets.\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "button": false,
    "nbgrader": {},
    "new_sheet": false,
    "run_control": {
     "frozen": false,
     "read_only": false
    },
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "# this is a code cell"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "button": false,
    "nbgrader": {},
    "new_sheet": false,
    "run_control": {
     "read_only": false
    },
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Command mode and edit mode"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "button": false,
    "nbgrader": {},
    "new_sheet": false,
    "run_control": {
     "read_only": false
    },
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "In Jupyter, each cell can be switched between two modes:\n",
    "\n",
    "<br>\n",
    "<dl>\n",
    "    <dt>Edit Mode</dt>\n",
    "    <dd>Allows you to modify the text or code</dd>\n",
    "    <dt>Command mode</dt>\n",
    "    <dd>Allows you to execute the code in a cell</dd>\n",
    "</dl>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "button": false,
    "nbgrader": {},
    "new_sheet": false,
    "run_control": {
     "read_only": false
    },
    "slideshow": {
     "slide_type": "skip"
    }
   },
   "source": [
    "By default the notebook begins in command mode.\n",
    "<div class=\"alert alert-info\">\n",
    "<b>When you are in command mode</b>, you can press <kbd>&#x23ce; Enter</kbd>to switch to edit mode. The left border of the cell you currently have selected will turn green, and a cursor will appear.\n",
    "</div>\n",
    "\n",
    "<div class=\"alert alert-info\">\n",
    "<b>When you are in edit mode</b>, you can press <kbd>Esc</kbd> to switch to command mode. The left border of the cell you currently have selected will turn blue, and the cursor will disappear.\n",
    "</div>\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "button": false,
    "nbgrader": {},
    "new_sheet": false,
    "run_control": {
     "read_only": false
    },
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Markdown cells\n",
    "\n",
    "Markdown cells can display formatted text and pictures, and are an excellent way to describe what your code does and discuss the results (imagine a lab report with interactive calculations). I strongly encourage you to use Markdown cells in your work!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "button": false,
    "nbgrader": {},
    "new_sheet": false,
    "run_control": {
     "read_only": false
    },
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "In **command mode** a markdown cell might look like this:\n",
    "\n",
    "![Markdown cell in command mode](images/command-mode-markdown-rendered.png)\n",
    "\n",
    "Then, when you press <kbd>&#x23ce; Enter</kbd>, it will change to **edit mode**:\n",
    "\n",
    "![Markdown cell in edit mode](images/edit-mode-markdown.png)\n",
    "\n",
    "Now, when we press <kbd>Esc</kbd>, it will change back to **command mode**:\n",
    "\n",
    "![Markdown cell in command mode after editing](images/command-mode-markdown-unrendered.png)\n",
    "\n",
    "Notice that the markdown cell no longer looks like it did originally; we need to **run** the cell in order to display the formatted markdown. Do this by pressing <kbd>Ctrl</kbd> and <kbd>&#x23ce; Enter</kbd>, and then it will go back to looking like it did originally:\n",
    "\n",
    "![Markdown cell after running the cell](images/command-mode-markdown-rendered.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Your turn!\n",
    "Test out this cell, switching between **command mode**, **edit mode** and **running** the cell."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "button": false,
    "nbgrader": {},
    "new_sheet": false,
    "run_control": {
     "read_only": false
    },
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Code cells\n",
    "\n",
    "Code cells contain code that should run, and possibly produce an output (such as graphs, tables, numbers, etc)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "button": false,
    "nbgrader": {},
    "new_sheet": false,
    "run_control": {
     "read_only": false
    },
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Similar to Markdown cells, code cells have both **command** mode:\n",
    "\n",
    "![Code cell in command mode](images/command-mode-outline.png)\n",
    "\n",
    "and **edit** mode (switch to **edit** mode using <kbd>&#x23ce; Enter</kbd>):\n",
    "\n",
    "![Code cell in edit mode](images/edit-mode-outline.png)\n",
    "\n",
    "Pressing escape will go back to **command mode** again:\n",
    "\n",
    "![Code cell in command mode again](images/command-mode-outline.png)\n",
    "\n",
    "The main difference between markdown and code cells is that if we press <kbd>Ctrl</kbd> and <kbd>&#x23ce; Enter</kbd>, this will **run** the code in the code cell:\n",
    "\n",
    "![Running code cell](images/code-cell-run.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "button": false,
    "nbgrader": {},
    "new_sheet": false,
    "run_control": {
     "read_only": false
    },
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Running code cells"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "button": false,
    "nbgrader": {},
    "new_sheet": false,
    "run_control": {
     "read_only": false
    },
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "Code cells can contain any valid Python code in them. When you run the cell, the code is executed and any output is displayed.\n",
    "\n",
    "<div class=\"alert alert-info\">\n",
    "    You can execute cells with <kbd>Ctrl</kbd> and <kbd>&#x23ce; Enter</kbd> which will keep the current cell selected, or <kbd>&#x21E7; Shift</kbd> and <kbd>&#x23ce; Enter</kbd> which will move to the next cell.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "button": false,
    "nbgrader": {},
    "new_sheet": false,
    "run_control": {
     "read_only": false
    },
    "slideshow": {
     "slide_type": "skip"
    }
   },
   "source": [
    "### Your turn:\n",
    "\n",
    "Try running the following cell and see what it prints out (don't worry about understanding the code for now):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "button": false,
    "nbgrader": {},
    "new_sheet": false,
    "run_control": {
     "frozen": false,
     "read_only": false
    },
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "print(\"Printing cumulative sum from 1-10:\")\n",
    "\n",
    "total = 0\n",
    "\n",
    "for i in range(1, 11):\n",
    "    total += i\n",
    "    print( f'Sum of 1 to {i} is {total}')\n",
    "\n",
    "print( 'Done printing numbers.' )"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "button": false,
    "nbgrader": {},
    "new_sheet": false,
    "run_control": {
     "read_only": false
    },
    "slideshow": {
     "slide_type": "skip"
    }
   },
   "source": [
    "You'll notice that the output beneath the cell corresponds to the `print` statements in the code. Here is another example which only prints out the final total."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "button": false,
    "nbgrader": {},
    "new_sheet": false,
    "run_control": {
     "frozen": false,
     "read_only": false
    },
    "slideshow": {
     "slide_type": "skip"
    }
   },
   "outputs": [],
   "source": [
    "total = 0\n",
    "\n",
    "for i in range(1, 11):\n",
    "    total += i\n",
    "\n",
    "print(total)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "button": false,
    "nbgrader": {},
    "new_sheet": false,
    "run_control": {
     "read_only": false
    },
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## The Jupyter kernel"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "button": false,
    "nbgrader": {},
    "new_sheet": false,
    "run_control": {
     "read_only": false
    },
    "slideshow": {
     "slide_type": "skip"
    }
   },
   "source": [
    "When you first start a notebook, you are also starting what is called a **kernel**. This is a special program that runs in the background and executes Python code. Whenever you run a code cell, you are telling the kernel to execute the code that is in the cell, and to print the output (if any)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "button": false,
    "nbgrader": {},
    "new_sheet": false,
    "run_control": {
     "read_only": false
    },
    "slideshow": {
     "slide_type": "skip"
    }
   },
   "source": [
    "### Restarting the kernel"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "button": false,
    "nbgrader": {},
    "new_sheet": false,
    "run_control": {
     "read_only": false
    },
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "If you are sure your code is correct but it keeps giving you errors then restarting the kernel often helps.\n",
    "\n",
    "The restart kernel button is the circular arrow in the toolbar (pointed to by the red arrow in this image).\n",
    "\n",
    "![](images/restart-kernel-button.png)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Help with Jupyer Notebooks"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "button": false,
    "nbgrader": {},
    "new_sheet": false,
    "run_control": {
     "read_only": false
    },
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "<div class=\"alert alert-info\">\n",
    "There are many keyboard shortcuts for the notebook. To see a full list of these, go to <b>Help$\\rightarrow$Keyboard Shortcuts</b>.\n",
    "</div>\n",
    "\n",
    "<div class=\"alert alert-info\">To learn a little more about what things are what in the Jupyter Notebook, check out the user interface tour, which you can access by going to <b>Help$\\rightarrow$User Interface Tour</b>.</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Tasks 1.1\n",
    "Please work through these tasks in groups of two or three. Don't worry if you don't finish the in the alloted time!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Task 1.1.1: Controlling Cells\n",
    "\n",
    "\n",
    "In the Jupyter notebook page are you currently in Command or Edit mode?\n",
    "Switch between the modes. \n",
    "- Use the shortcuts to generate a new cell. \n",
    "- Use the shortcuts to delete a cell. \n",
    "- Use the shortcuts to undo the last cell operation you performed."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "#: Your answer or a result here"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Task 1.1.2: Markdown lists\n",
    "\n",
    "Create a nested list in a Markdown cell in a notebook that looks similar to this (the bullet design will depend on your browser settings):\n",
    "![nest_list](images/nested_list.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "#: Your answer or a result here"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Task 1.1.3: Maths in code cells\n",
    "What happens if you write some Maths in a code cell and execute it. What happens? What happens if you then switch it to a Markdown cell? For example, put the following in a code cell:\n",
    "\n",
    "```Python\n",
    "6*7+12\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "#: Your answer or a result here"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Task 1.1.4: Equations\n",
    "\n",
    "Standard Markdown (such as we’re using for these notes) won’t render equations, but the Notebook will. Create a new Markdown cell and enter the following:\n",
    "`$$\\sum_{i=1}^{N} 2^{-i} \\approx 1$$`\n",
    "(It’s probably easier to copy and paste.) What does it display? What do you think the underscore (_), circumflex (^) and dollar signs ($) do?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "#: Your answer or a result here"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "skip"
    }
   },
   "source": [
    "### Task 1.1.5: Closing and Restarting\n",
    "\n",
    "- Close your Jupyter notebook and restart it. \n",
    "- Restart the Kernel\n",
    "- Try opening a blank notebook\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "skip"
    }
   },
   "source": [
    "### 1.1 Advanced Tasks\n",
    "\n",
    "Try to create a table using a markdown cell, containing for instance the formula masses of the first 6 elements."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "skip"
    }
   },
   "source": [
    "## Next Notebook\n",
    "\n",
    "[Introduction to problem solving](Session_1.2_problem_solving.ipynb)"
   ]
  }
 ],
 "metadata": {
  "anaconda-cloud": {},
  "hide_input": false,
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.12"
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": false,
   "sideBar": true,
   "skip_h1_title": false,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {},
   "toc_section_display": true,
   "toc_window_display": true
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
